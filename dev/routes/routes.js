import MainController from "../controllers/MainController";
import SecuredController from "../controllers/SecuredController";
import StandardAuthenticationPolicy from "../policies/StandardAuthenticationPolicy";

import TestRestController from '../controllers/TestRestController';

let standardAuthenticationStrategy = new StandardAuthenticationPolicy();

const routesValues = [
    {
        "methods": ["get"],
        "controller": MainController.index
    },
    {
        "path": "/model",
        "controllerClass": TestRestController,
        "arguments": ["id"]
    },
    {
        "path": "/hello",
        "arguments": ["name"],
        "methods": ["get"],
        "controller": MainController.sayHello
    },
    {
        "path": "/secure",
        "methods": ["get"],
        "controller": SecuredController.index,
        "policy": standardAuthenticationStrategy
    },
    {
        "arguments": ["name"],
        "methods": ["get"],
        "controller": MainController.sayHello
    },
    {
        "arguments": ["name"],
        "methods": ["post", "put"],
        "controller": MainController.postTest
    }
];

export default routesValues;