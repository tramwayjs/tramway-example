import {controllers} from 'tramway-core';
import TestModel from '../models/TestModel';

let {RestfulController} = controllers;

export default class TestRestController extends RestfulController {
    static get(req, res) {
        let testModel = (new TestModel()).setId(req.params.id);
        return super.get(testModel, req, res);
    }

    static getAll(req, res) {
        let testModel = new TestModel();
        return super.getAll(testModel, req, res);
    }

    static create(req, res) {
        let testModel = new TestModel().updateEntity(req.body);
        return super.create(testModel, req, res);
    }

    static update(req, res) {
        let testModel = new TestModel().updateEntity(req.body).setId(req.params.id);
        return super.update(testModel, req, res);
    }

    static delete(req, res) {
        let testModel = new TestModel().setId(req.params.id);
        return super.delete(testModel, req, res);
    }
}