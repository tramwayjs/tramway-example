import {Controller} from 'tramway-core';
import os from 'os';

/**
 * @class MainController
 * @extends {Controller}
 */
export default class MainController extends Controller {
    /**
     * @static
     * @param {Object} req
     * @param {Object} res
     * @memberOf Main
     */
    static index (req, res) {
        res.send(`Page rendered from ${os.hostname()}`);
    }

    /**
     * @static
     * @param {Object} req
     * @param {Object} res
     * @memberOf Main
     */
    static sayHello(req, res) {
        res.send(`Hello ${req.params.name}`);
    }

    static postTest(req, res) {
        res.json({name: req.params.name, body: req.body});
    }
}