'use strict';

import express from 'express';
import bodyParser from 'body-parser';
import cors from './config/cors.js';
import methodOverride from 'method-override';
import cookieParser from 'cookie-parser';
import {Router, strategies} from 'tramway-core-router';
import routes from './routes/routes.js';
let {ExpressServerStrategy} = strategies;

const PORT = 8080;

let app = express();
app.use(methodOverride('_method'));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(cors);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

let router = new Router(routes, new ExpressServerStrategy(app));
app = router.initialize();

app.listen(PORT);
console.log(`Started on port ${PORT}`);

export default app;