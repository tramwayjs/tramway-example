import {Entity} from "tramway-core";

export default class TestEntity extends Entity {
    /**
     * Creates an instance of A.
     * @memberOf A
     */
    constructor(){
        super();
        this.id = null;
    }
    /**
     * @param {number} value
     * @returns {TestEntity}
     * 
     * @memberOf TestEntity
     */
    setId(id) {
        this.id = id;
        return this;
    }
    /**
     * 
     * @returns {number}
     * 
     * @memberOf TestEntity
     */
    getId() {
        return this.id;
    }
}